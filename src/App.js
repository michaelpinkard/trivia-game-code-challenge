import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import TopLevelCard from "./Components/TopLevelCard";
import CheckAnswersDialog from "./Components/CheckAnswersDialog";

class App extends Component {
  constructor(props) {
    super(props);
    this.slider = React.createRef();

    //Bind function context to the App component
    this.playAgain = this.playAgain.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.checkAnswers = this.checkAnswers.bind(this);
    this.returnToMissedQuestions = this.returnToMissedQuestions.bind(this);
    this.handleCloseDialog = this.handleCloseDialog.bind(this);
    this.handleTrueFalseClick = this.handleTrueFalseClick.bind(this);
    this.handleNextPrevClick = this.handleNextPrevClick.bind(this);

    //Initiate state
    this.state = { index: 0, showResultsPage: false, loading: true };
  }

  fetchNewDataAndReset() {
    this.setState({ loading: true });
    fetch("https://opentdb.com/api.php?amount=10&difficulty=hard&type=boolean")
      .then(this.checkStatus)
      .then(this.parseJSON)
      .then(data => {
        const questions = data.results;
        this.setState({
          index: 0,
          showResultsPage: false,
          loading: false,
          questions,
          answers: {}
        });
      })
      .catch(function(error) {
        console.log("request failed", error);
      });
  }

  componentDidMount() {
    this.fetchNewDataAndReset();
  }

  checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response;
    } else {
      var error = new Error(response.statusText);
      error.response = response;
      throw error;
    }
  }

  parseJSON(response) {
    return response.json();
  }

  handleTrueFalseClick(answer, index, event) {
    this.setState(prevState => ({
      answers: Object.assign({}, prevState.answers, {
        [index]: answer
      })
    }));
  }

  handleNextPrevClick(index) {
    this.setState({ index });
  }

  handleCloseDialog() {
    this.setState({
      openCheckAnswersDialog: false
    });
  }

  /*
  Here we implement an algorithm to find the first question that has not been answered. We check for the edge cases where:
  a) No questions have been answered
  b) Only one question has been answered
  c) All the questions have been answered
  Then we walk through the list of indices of
  answered questions, looking for a gap, which 
  is equivalent to a place where the index in
  question and the next index are more than
  distance 1 apart. We return that index + 1
  to find the first missed question. */
  findFirstMissingQuestion() {
    const { answers, questions } = this.state;
    const answeredQuestions = Object.keys(answers)
      .map(Number)
      .sort();
    if (answeredQuestions.length < 1) {
      return 0;
    }
    if (answeredQuestions.length < 2) {
      return answeredQuestions[0] + 1;
    }
    if (answeredQuestions.length === questions.length) {
      return questions.length - 1;
    }
    for (let i = 0; i < answeredQuestions.length - 1; i++) {
      if (answeredQuestions[i + 1] - answeredQuestions[i] > 1) {
        return answeredQuestions[i] + 1;
      }
    }
    return answeredQuestions[answeredQuestions.length - 1] + 1;
  }

  returnToMissedQuestions() {
    this.slider.current.slickGoTo(this.findFirstMissingQuestion());
    this.setState({
      openCheckAnswersDialog: false
    });
  }

  checkAnswers() {
    this.setState({
      loading: true,
      openCheckAnswersDialog: false
    });
    setTimeout(() => {
      this.setState({
        loading: false,
        showResultsPage: true
      });
    }, 1000);
  }

  handleSubmit() {
    const { answers, questions } = this.state;
    if (Object.keys(answers).length === questions.length) {
      this.checkAnswers();
    } else {
      this.setState({ openCheckAnswersDialog: true });
    }
  }

  playAgain() {
    this.fetchNewDataAndReset();
  }

  render() {
    const { answers, questions, index, loading, showResultsPage } = this.state;
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to the Trivia Game</h1>
        </header>
        {!this.state.loading && (
          <h3 className="subtitle">{`There are ${
            this.state.questions.length
          } questions, good luck!`}</h3>
        )}
        {this.state.openCheckAnswersDialog && (
          <CheckAnswersDialog
            open={this.state.openCheckAnswersDialog}
            numberQuestionsAnswered={Object.keys(answers).length}
            handleCloseDialog={this.handleCloseDialog}
            returnToMissedQuestions={this.returnToMissedQuestions}
            checkAnswers={this.checkAnswers}
          />
        )}
        <TopLevelCard
          ref={this.slider}
          questions={questions}
          answers={answers}
          index={index}
          loading={loading}
          showResultsPage={showResultsPage}
          handleNextPrevClick={this.handleNextPrevClick}
          handleTrueFalseClick={this.handleTrueFalseClick}
          handleSubmit={this.handleSubmit}
          playAgain={this.playAgain}
        />
      </div>
    );
  }
}

export default App;
