import React from "react";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import DomPurify from "dompurify";

const QuestionContainer = ({ questionText, questionNumber }) => {
  return (
    <div className="question-container">
      <Typography className="headline" variant="headline" component="h4">
        {`Question ${questionNumber}`}
      </Typography>
      {/* In reference to the dangerouslySetInnerHtml and DomPurify.sanitize(),
      see the following StackOverflow question:
      https://stackoverflow.com/questions/19266197/reactjs-convert-to-html.
      Also see https://github.com/cure53/DOMPurify.
      In short, DomPurify takes care of the possibility of XSS.*/}
      <div className="question-text-flexbox">
        <Typography
          className="question-text-question-card"
          component="p"
          dangerouslySetInnerHTML={{ __html: DomPurify.sanitize(questionText) }}
        />
      </div>
    </div>
  );
};

QuestionContainer.propTypes = {
  questionText: PropTypes.string.isRequired,
  questionNumber: PropTypes.number.isRequired
};

export default QuestionContainer;
