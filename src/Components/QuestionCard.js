import React from "react";
import PropTypes from "prop-types";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import "./QuestionContainer";
import QuestionContainer from "./QuestionContainer";
import Slider from "react-slick";

const QuestionCard = React.forwardRef(
  (
    {
      questions,
      index,
      handleNextPrevClick,
      handleTrueFalseClick,
      handleSubmit,
      answers
    },
    slider
  ) => {
    const slickSettings = {
      dots: false,
      infinite: false,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      afterChange: next => handleNextPrevClick(next)
    };
    const next = () => {
      slider.current.slickNext();
    };
    const previous = () => {
      slider.current.slickPrev();
    };
    return (
      <div className="question-card">
        <div className="card-flexbox">
          <Card className="card-component">
            <Slider ref={slider} {...slickSettings}>
              {questions.map((question, i) => {
                const { [i]: answer } = answers;
                return (
                  <div className="cardContentContainer" key={i}>
                    <CardContent>
                      <QuestionContainer
                        questionText={question.question}
                        questionNumber={i + 1}
                      />
                    </CardContent>
                    <div className="answer-div">
                      <div className="button-container">
                        <Button
                          variant="contained"
                          color={answer ? "primary" : "secondary"}
                          onClick={() => handleTrueFalseClick(true, i)}
                        >
                          True
                        </Button>
                      </div>
                      <div className="button-container">
                        <Button
                          variant="contained"
                          color={
                            !answer && answers.hasOwnProperty(i)
                              ? "primary"
                              : "secondary"
                          }
                          onClick={event =>
                            handleTrueFalseClick(false, i, event)
                          }
                        >
                          False
                        </Button>
                      </div>
                    </div>
                  </div>
                );
              })}
            </Slider>
          </Card>
        </div>
        <div className="next-previous-button-container">
          <div className="next-previous-button-flexbox">
            {index > 0 ? (
              <div className="button-container">
                <Button
                  className="previous-button"
                  color="secondary"
                  onClick={previous}
                >
                  Previous
                </Button>
              </div>
            ) : (
              <div className="previous-button button-container" />
            )}
            <div className="button-container">
              <Button
                className="check-answers-button"
                color="primary"
                variant="contained"
                onClick={handleSubmit}
              >
                {Object.keys(answers).length === questions.length
                  ? "Submit"
                  : "Check Answers"}
              </Button>
            </div>
            {index < questions.length - 1 ? (
              <div className="button-container">
                <Button color="primary" onClick={next}>
                  Next
                </Button>
              </div>
            ) : (
              <div />
            )}
          </div>
        </div>
      </div>
    );
  }
);

QuestionCard.propTypes = {
  questions: PropTypes.arrayOf(
    PropTypes.shape({
      category: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
      difficulty: PropTypes.string.isRequired,
      question: PropTypes.string.isRequired,
      correct_answer: PropTypes.string.isRequired,
      incorrect_answers: PropTypes.arrayOf(PropTypes.string).isRequired
    })
  ).isRequired,
  index: PropTypes.number.isRequired,
  answers: PropTypes.objectOf(PropTypes.bool).isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleTrueFalseClick: PropTypes.func.isRequired,
  handleNextPrevClick: PropTypes.func.isRequired
};

export default QuestionCard;
