import React from "react";
import PropTypes from "prop-types";
import purple from "@material-ui/core/colors/purple";
import CircularProgress from "@material-ui/core/CircularProgress";
import QuestionCard from "./QuestionCard";
import ResultsCard from "./ResultsCard";

const TopLevelCard = React.forwardRef(
  (
    {
      questions,
      index,
      showResultsPage,
      answers,
      loading,
      playAgain,
      handleNextPrevClick,
      handleSubmit,
      handleTrueFalseClick
    },
    slider
  ) => {
    if (loading) {
      return (
        <div className="progress-container">
          <CircularProgress
            className="progress-spinner"
            style={{ color: purple[500] }}
            thickness={7}
          />
        </div>
      );
    } else if (questions && showResultsPage) {
      return (
        <ResultsCard
          questions={questions}
          answers={answers}
          playAgain={playAgain}
        />
      );
    } else if (questions) {
      return (
        <QuestionCard
          ref={slider}
          questions={questions}
          answers={answers}
          index={index}
          handleNextPrevClick={handleNextPrevClick}
          handleTrueFalseClick={handleTrueFalseClick}
          handleSubmit={handleSubmit}
        />
      );
    } else {
      return null;
    }
  }
);

TopLevelCard.propTypes = {
  index: PropTypes.number.isRequired,
  questions: PropTypes.arrayOf(
    PropTypes.shape({
      category: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
      difficulty: PropTypes.string.isRequired,
      question: PropTypes.string.isRequired,
      correct_answer: PropTypes.string.isRequired,
      incorrect_answers: PropTypes.arrayOf(PropTypes.string).isRequired
    })
  ).isRequired,
  loading: PropTypes.bool.isRequired,
  showResultsPage: PropTypes.bool.isRequired,
  answers: PropTypes.objectOf(PropTypes.bool).isRequired,
  playAgain: PropTypes.func.isRequired,
  handleNextPrevClick: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  handleTrueFalseClick: PropTypes.func.isRequired
};

export default TopLevelCard;
