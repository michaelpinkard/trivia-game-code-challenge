import React from "react";
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import ResultContainer from "./ResultContainer";
import identity from "lodash/identity";
import get from "lodash/get";
/* Can improve webpack performance 
and reduce the build size when we
do lodash imports like this:
https://nolanlawson.com/2018/03/20/smaller-lodash-bundles-with-webpack-and-babel/ */

const ResultsCard = ({ questions, answers, playAgain }) => {
  const results = questions.map((question, index) => {
    const correctAnswer = question.correct_answer === "True";
    const answer = get(answers, index);
    return correctAnswer === answer;
  });
  const score = results.filter(identity).length;
  return (
    <div className="card-flexbox">
      <Paper className="card-component">
        <Typography className="headline" variant="headline" component="h4">
          {`You scored ${score}`}
        </Typography>
        <div>
          {questions.map((question, index) => {
            const result = results[index];
            const correctAnswer = question.correct_answer;
            return (
              <ResultContainer
                questionText={question.question}
                correct={result}
                correctAnswer={correctAnswer}
                key={index}
              />
            );
          })}
        </div>
        <Button variant="contained" color="primary" onClick={playAgain}>
          Play Again
        </Button>
      </Paper>
    </div>
  );
};

ResultsCard.propTypes = {
  questions: PropTypes.arrayOf(
    PropTypes.shape({
      category: PropTypes.string.isRequired,
      type: PropTypes.string.isRequired,
      difficulty: PropTypes.string.isRequired,
      question: PropTypes.string.isRequired,
      correct_answer: PropTypes.string.isRequired,
      incorrect_answers: PropTypes.arrayOf(PropTypes.string).isRequired
    })
  ).isRequired,
  answers: PropTypes.objectOf(PropTypes.bool),
  playAgain: PropTypes.func.isRequired
};

export default ResultsCard;
