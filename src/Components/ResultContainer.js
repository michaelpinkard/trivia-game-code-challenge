import React from "react";
import PropTypes from "prop-types";
import DomPurify from "dompurify";

const ResultContainer = ({ questionText, correct, correctAnswer }) => {
  return (
    <div className="result-container">
      <div
        className="symbol"
        dangerouslySetInnerHTML={{ __html: correct ? `+` : `&#8211;` }}
      />
      {/* In reference to the dangerouslySetInnerHtml  and DomPurify.sanitize(),
      see the following StackOverflow question:
      https://stackoverflow.com/questions/19266197/reactjs-convert-to-html.
      Also see https://github.com/cure53/DOMPurify.
      In short, DomPurify takes care of the possibility of XSS.*/}
      <div className="answer-text-flexbox">
        <div
          className="question-text-results-card"
          dangerouslySetInnerHTML={{ __html: DomPurify.sanitize(questionText) }}
        />
        <div className="correct-answer-text">{correctAnswer}</div>
      </div>
    </div>
  );
};

ResultContainer.propTypes = {
  questionText: PropTypes.string.isRequired,
  correct: PropTypes.bool.isRequired
};

export default ResultContainer;
