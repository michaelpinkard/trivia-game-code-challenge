import React from "react";

import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";

const CheckAnswersDialog = ({
  open,
  numberQuestionsAnswered,
  handleCloseDialog,
  returnToMissedQuestions,
  checkAnswers
}) => {
  return (
    <Dialog
      open={open}
      onClose={handleCloseDialog}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Check your answers?"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {`You have only answered ${numberQuestionsAnswered} out of 10 questions. Do you still want to check your answers?`}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleCloseDialog} color="primary">
          Cancel
        </Button>
        <Button onClick={returnToMissedQuestions} color="primary">
          Go Back to Missed Questions
        </Button>
        <Button onClick={checkAnswers} color="primary" autoFocus>
          Check Answers
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default CheckAnswersDialog;
